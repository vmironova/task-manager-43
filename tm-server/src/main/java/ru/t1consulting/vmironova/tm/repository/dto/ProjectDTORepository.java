package ru.t1consulting.vmironova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1consulting.vmironova.tm.api.repository.dto.IProjectDTORepository;
import ru.t1consulting.vmironova.tm.dto.model.ProjectDTO;

import javax.persistence.EntityManager;

public final class ProjectDTORepository extends AbstractUserOwnedDTORepository<ProjectDTO> implements IProjectDTORepository {

    public ProjectDTORepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public ProjectDTO create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) throws Exception {
        @NotNull final ProjectDTO project = new ProjectDTO(name, description);
        return add(userId, project);
    }

    @NotNull
    @Override
    public ProjectDTO create(
            @NotNull final String userId,
            @NotNull final String name
    ) throws Exception {
        @NotNull final ProjectDTO project = new ProjectDTO(name);
        return add(userId, project);
    }

}
